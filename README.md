# Spite

[Spite Tech](https://spitetech.com) is a website providing private solutions to popular software that are *secure* simultaneously. We believe there should be a happy medium with the two qualities as they are similar to that of the Yin and Yang. True privacy cannot be accomplished without a competent layer of security, and often times than not users make compromise security for "privacy". We're here to fix that.

# To-Do List

(Not particularly in chronological order)
- Make website mobile-friendly, as it was initially designed for desktop only in mind.
- Make the hover color darker/more noticeable to make it more user-friendly.
- Remove the about page and merge it with the landing page, they were originally separated to make a test page and because the website wasn't planned to have content within it.
- Remodel and republish the "conduct" page.